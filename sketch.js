// create variables
let height = 800
let width = 400
let pointX = 10
let pointY = 10
let value = 1
let write = 0

// create canvas
function setup() {
  createCanvas(height, width);
}


// loop
function draw() {
  background(220);
  line(height/2 ,0 ,height/2 ,width);

  if (value == 1){
    pointX = mouseX;
    pointY =  mouseY;
  }

  if (pointX > height) {pointX=height}
  if (pointY > width) {pointY=width} 

  //Draw the point
  let xc = constrain(pointX, 6, height-6);
  let yc = constrain(pointY, 6, width-6);
  circle (xc,yc, 10);


//Print the X, Y, and the Results.
  textSize(20)
  text (`X= ${pointX}`, 5, 20)
  text (`Y= ${pointY}`, 5, 40)
}

//Put the point.
function mouseClicked() {
  value = 0;
  pointX = mouseX;
  pointY =  mouseY;
}

function doubleClicked() {
  value = 1;
}


function keyPressed() {
  if (keyCode === ENTER) {
    value = 0;
    pointX = window.prompt("Enter x: ");
    pointY = window.prompt("Enter Y: ");
  }

}
